#  Write python function which return whether given number is prime or not.

def my_fun(num):
    
    for i in range(2, num):
        if (num % i) == 0:
            return False
    return True

print(my_fun(4))