from turtle import right


class Triangle:
    right_angled = True

    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c
    
    def perimetr(self):
        perimetr = self.a + self.b + self.c
        return perimetr

    def area(self):
        area = (self.a * self.b) // 2
        return area

triangle = Triangle(10, 5, 5)     
print(triangle.perimetr(), triangle.area())   