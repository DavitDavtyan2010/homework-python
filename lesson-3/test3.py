# 3. Write a Python program to remove the n-th index character from a nonempty string.

string = 'abcdefg'
n = 3
new_string = string[:n] + string[-n:]

print(new_string)