# 9. Append new string in the middle of a given (even number of characters) string

string1 = 'language'
string2 = 'new'
mixed_string = string1[:len(string1) // 2] + string2 + string1[len(string1) // 2:]

print(mixed_string)