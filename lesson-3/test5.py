# 5. Write a Python function to get a string made of 4 copies of the last two characters of a
# specified string (length must be at least 2)

def my_fun(string):
    print(string[-2:] * 3)

my_fun('Python')