#  Write a Python program to square and cube every number in a given list of
# integers using Lambda.

numbers = (1,2,3)

res_1 = map(lambda x: x * x, numbers)

res_2 = map(lambda x: x * x * x, numbers)

print(list(res_1), list(res_2))