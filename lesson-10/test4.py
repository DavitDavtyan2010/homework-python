# Write a Python program to find intersection of two given arrays using
# Lambda.

first = (1,2,3,5,8,7,6)
second = (1,3,4)

res = filter(lambda x: x in second , first)

print(list(res))