# Write a Python program to find if a given string starts with a given
# character using Lambda.

string = '10000'

res = map(lambda x: True if x.startswith('1') else False, string)
print(list(res))