# Write a Python program to add two given lists using map and
# lambda.

first = (1, 2, 3)
second = (4, 5, 6)

res = map(lambda x: x[0] + second[0], first)

print(list(res))

# it is don not working