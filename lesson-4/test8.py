#  Given the salaries of three employees working at a department, find the
# amount of money by which the salary of the highest-paid employee differs
# from the salary of the lowest-paid employee. The input consists of three
# positive integers - the salaries of the employees. Output a single number,
# the difference between the top and the bottom salaries

list = (100, 100, 100)

if list[2] > list[1] > list[0] :
    print(list[2] - list[0])
elif list[2] > list[1] < list[0] and list[2] > list[0] :
    print(list[2] - list[1])
elif list[2] > list[1] < list[0] and list[2] < list[0] :
    print(list[0] - list[1])
elif list[0] > list[2] < list[1] and list[1] > list[0] :
    print(list[1] - list[0])
elif list[0] > list[2] < list[1] and list[0] > list[1] :
    print(list[0] - list[1])
elif list[0] > list[1] > list[2] :
    print(list[0] - list[2])
elif list[0] < list[1] > list[2] and list[2] > list[0] :
    print(list[1] - list[0])
elif list[0] < list[1] > list[2] and list[2] < list[0] :
    print(list[1] - list[2])
elif list[0] == list[1] == list[2] :
    print(0)
