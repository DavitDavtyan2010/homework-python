# Input a two-digit natural number and output the sum of its digits. You can assume that the input will be a two-digit number and need not check that programmatically.

a = 66
b = a % 10
c = a // 10

print(c + b)