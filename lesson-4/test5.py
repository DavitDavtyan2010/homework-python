# The program prompts the user their birth year. Assuming a person’s age
# is a non-negative integer not exceeding 120, output the user’s age or the
# words “Incorrect Year”. The sample outputs assume it’s currently the year
# 2016. If you are writing this program during any other year, the correct
# answers may differ. Store the value of the current year in a variable.

a = input('Enter your Birth year ')

if int(a) > 2022  :
    print('Incorrect Year')
elif int(a) < 2022 and 2022 - int(a) <= 120 :
    b = 2022 - int(a)
    print('you are ' + str(b) + ' years old')
elif 2022 - int(a) >= 120 :
    print('you cannot be 120 year or older')