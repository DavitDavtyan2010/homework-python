# Given a real number, round it to the nearest whole.

a = 10.7

if int(a % 10) >= 5 :
    print((a - (a % 10)) + 1)
elif a % 10 < 5 :
    print(int(a - (a % 10)))

