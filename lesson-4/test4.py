# Input two positive integers, and output a line describing their relation. Follow the sample format.

a = 12
b = 6

if a < b :
    print('a < b')
elif a > b :
    print('a > b')
else:
    print('a = b')