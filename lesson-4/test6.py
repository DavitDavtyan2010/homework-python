# Input three integers. Output the word “Sorted” if the numbers are listed in
# a non-increasing or non-decreasing order and “Unsorted” otherwise.

list = [1, 2, 3]

if list[0] >= list[1] and list[1] >= list[2] :
    print('sorted')
elif list[0] <= list[1] and list[1] <= list[2] :
    print('sorted')
elif list[2] < list[1] and list[0] < list[1] :
    print('unsorted')
else:
    print('unsorted')