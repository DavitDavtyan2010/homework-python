# Generate a random Password which meets the following conditions
#   ● Password length must be 10 characters long.
#   ● It must contain at least 2 upper case letters, 1 digit, and 1 special symbol.


import random

letters_low = 'abcdefghijklmnopqrstuvwxyz'
letters_up = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
numbers = '123456789'
symbols = '!@#$%^&*()/\\'

print(random.choice(symbols) + random.choice(letters_up) + random.choice(letters_low) + random.choice(letters_up) + random.choice(numbers) + random.choice(symbols) + random.choice(numbers) + random.choice(letters_up) + random.choice(letters_up) + random.choice(letters_low))