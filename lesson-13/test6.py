#.Write a Python program to convert a datetime.now() like output format.

import random

year = 2022
datetime = '2000-05-18 19:34:17.545'
new_year = random.randint(int(datetime[0:4]), year)
new_month = random.randint(int(datetime[6:7]), 12)
new_day = random.randint(int(datetime[8:10]), 28)
new_time = random.randint(int(datetime[11:12]), 24)
new_minute = random.randint(int(datetime[14:16]), 60)
new_second = random.randint(int(datetime[17:19]), 60)
new_milisecond = random.randint(int(datetime[20:23]), 999)

new_datetime = f'{new_year}-{new_month}-{new_day} {new_time}:{new_minute}:{new_second}.{new_milisecond}'

print(new_datetime)