#Write a python program to catch error and print “Error is catched”
#You need to do it with 5 different Errors

try:
    L1 = [1,2,3]
    L1[3]
except IndexError:
    print('catched error')

try:
    D1 = {'1':"aa", '2':"bb", '3':"cc"}
    D1['4']
except KeyError:
    print('catched error')

try:
    '2' + 2
except TypeError:
    print('catched error')

try:
    x = 100/0
except ZeroDivisionError:
    print('catched error') 

try:
    import notamodule
except ModuleNotFoundError:
    print('catched error') 